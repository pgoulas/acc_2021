FROM openjdk:8
EXPOSE 8080
ADD target/matches-project.jar matches-project.jar
ENTRYPOINT ["java", "-jar", "/matches-project.jar"]

package com.matches.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.matches.entity.Odds;

public interface OddsRepository extends JpaRepository<Odds, Long> {

	Page<Odds> findAllByMatch_Id(Long id, Pageable pageable);

	List<Odds> findAllById(Long id);
}

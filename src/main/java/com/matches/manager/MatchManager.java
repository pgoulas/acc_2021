package com.matches.manager;

import java.text.MessageFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.matches.entity.Match;
import com.matches.repository.MatchRepository;

@Component
public class MatchManager {

	@Autowired
	private MatchRepository matchRepository;

	public Match getMatchById(Long id) throws Exception {

		List<Match> matches = matchRepository.findAllById(id);
		if (matches.isEmpty()) {
			MessageFormat messageFormat = new MessageFormat("Match with id={0} is not found");
			String message = messageFormat.format(new Object[]{id});
			throw new Exception(message);
		}
		return matches.get(0);
	}

	public Match registerOrUpdate(Match match) {
		return matchRepository.save(match);
	}

	public Page<Match> getMatches(Pageable pageable) {
		return matchRepository.findAll(pageable);
	}

	public Long removeMatch(Long id) {
		matchRepository.deleteById(id);
		return id;
	}

}

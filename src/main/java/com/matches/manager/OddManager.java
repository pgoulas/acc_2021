package com.matches.manager;

import java.text.MessageFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.matches.entity.Odds;
import com.matches.repository.OddsRepository;

@Component
public class OddManager {

	@Autowired
	private OddsRepository oddsRepository;

	public Odds getMatchById(Long id) throws Exception {
		List<Odds> odds = oddsRepository.findAllById(id);
		if (odds.isEmpty()) {
			MessageFormat messageFormat = new MessageFormat("Odd with id={} is not found");
			throw new Exception(messageFormat.format(new Object[]{id}));
		}

		return odds.get(0);
	}

	public Long removeOdd(Integer id) throws Exception {
		Odds odds = getMatchById(id.longValue());
		oddsRepository.delete(odds);
		return id.longValue();
	}

	public Odds registerOrUpdate(Odds odds) {
		return oddsRepository.save(odds);
	}

}

package com.matches.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.matches.entity.Match;
import com.matches.model.MatchDto;
import com.matches.model.MatchResponseDto;
import com.matches.service.MatchService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController(value = "match")
@RequestMapping("/match")
public class MatchController {

	@Autowired
	private MatchService matchService;

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	@ApiOperation(
		value = "Get all the matches",
		notes = "Get all the matches")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Success", response = MatchResponseDto.class),
		@ApiResponse(code = 500, message = "Runtime Fault", response = String.class),
		@ApiResponse(code = 404, message = "Not found", response = String.class)
	})
	public MatchResponseDto getMatches(
		@ApiParam(name = "page", type = "Integer", value = "Page", example = "1", required = true) @RequestParam Integer page,
		@ApiParam(name = "limit", type = "Integer", value = "Limit", example = "10", required = true) @RequestParam Integer limit,
		@ApiParam(name = "sort", type = "String", value = "Sort", example = "asc", required = true) @RequestParam String sort
	) {
		return matchService.getMatches(page, limit, sort);

	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(
		value = "Insert a new match",
		notes = "Insert a new match")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Success", response = Match.class),
		@ApiResponse(code = 500, message = "Runtime Fault", response = String.class),
		@ApiResponse(code = 404, message = "Not found", response = String.class)
	})
	public Match insertMatch(@RequestBody MatchDto matchDto) {
		return matchService.createMatch(matchDto);

	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	@ApiOperation(
		value = "Update an existing match",
		notes = "Update an existing match")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Success", response = Match.class),
		@ApiResponse(code = 500, message = "Runtime Fault", response = String.class),
		@ApiResponse(code = 404, message = "Not found", response = String.class)
	})
	public Match updateMatch(@PathVariable Integer id, @RequestBody MatchDto matchDto) throws Exception {
		try {
			return matchService.updateMatch(id.longValue(), matchDto);
		} catch (Exception e) {
			throw new Exception("Bad String");
		}

	}

	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	@ApiOperation(
		value = "Delete a match",
		notes = "Delete a match")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Success", response = Long.class),
		@ApiResponse(code = 500, message = "Runtime Fault", response = String.class),
		@ApiResponse(code = 404, message = "Not found", response = String.class)
	})
	public Long removeMatch(@PathVariable Integer id) throws Exception {
		try {
			return matchService.removeMatch(id.longValue());
		} catch (Exception e) {
			throw new Exception("Bad String");
		}

	}
}


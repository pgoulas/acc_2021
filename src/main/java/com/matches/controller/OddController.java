package com.matches.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.matches.entity.Match;
import com.matches.entity.Odds;
import com.matches.model.OddDto;
import com.matches.service.MatchService;
import com.matches.service.OddService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController(value = "odds")
@RequestMapping("/odd")
public class OddController {

	@Autowired
	private MatchService matchService;

	@Autowired
	private OddService oddService;

	@RequestMapping(value = "/all/{id}", method = RequestMethod.GET)
	@ApiOperation(
		value = "Get all the odds by given match id",
		notes = "Get all the odds by given match id")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Success", response = Odds.class, responseContainer = "List"),
		@ApiResponse(code = 500, message = "Runtime Fault", response = String.class),
		@ApiResponse(code = 404, message = "Not found", response = String.class)
	})
	public List<Odds> getOdds(@PathVariable Integer id) {
		return matchService.getOddsByMatchId(id);

	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(
		value = "Insert a new odd",
		notes = "Insert a new odd")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Success", response = Match.class),
		@ApiResponse(code = 500, message = "Runtime Fault", response = String.class),
		@ApiResponse(code = 404, message = "Not found", response = String.class)
	})
	public Match insertOdd(@RequestBody OddDto oddDto) throws Exception {
		return oddService.insertOddToMatch(oddDto);

	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	@ApiOperation(
		value = "Update an existing odd",
		notes = "Update an existing odd")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Success", response = Match.class),
		@ApiResponse(code = 500, message = "Runtime Fault", response = String.class),
		@ApiResponse(code = 404, message = "Not found", response = String.class)
	})
	public Odds updateOdd(@PathVariable Integer id, @RequestBody OddDto oddDto) throws Exception {
		return oddService.updateOdd(id, oddDto);

	}

	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	@ApiOperation(
		value = "Delete a odd",
		notes = "Delete a odd")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Success", response = String.class),
		@ApiResponse(code = 500, message = "Runtime Fault", response = String.class),
		@ApiResponse(code = 404, message = "Not found", response = String.class)
	})
	public Long removeOdd(@PathVariable Integer id) throws Exception {
		return oddService.removeOdd(id);

	}
}

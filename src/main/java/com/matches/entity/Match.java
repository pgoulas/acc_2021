package com.matches.entity;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.matches.enumeration.Sport;

@Entity
@Table(name = "match")
public class Match {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "MATCH_DATE")
	private LocalDate matchDate;

	@Column(name = "MATCH_TIME")
	private LocalTime matchTime;

	@Column(name = "TEAM_A")
	private String teamA;

	@Column(name = "TEAM_B")
	private String teamB;

	@Column(name = "SPORT")
	private Sport sport;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "match", cascade = CascadeType.ALL, orphanRemoval = true)
	private final Collection<Odds> odds = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public LocalDate getMatchDate() {
		return matchDate;
	}

	public void setMatchDate(final LocalDate matchDate) {
		this.matchDate = matchDate;
	}

	public LocalTime getMatchTime() {
		return matchTime;
	}

	public void setMatchTime(final LocalTime matchTime) {
		this.matchTime = matchTime;
	}

	public String getTeamA() {
		return teamA;
	}

	public void setTeamA(final String teamA) {
		this.teamA = teamA;
	}

	public String getTeamB() {
		return teamB;
	}

	public void setTeamB(final String teamB) {
		this.teamB = teamB;
	}

	public Sport getSport() {
		return sport;
	}

	public void setSport(final Sport sport) {
		this.sport = sport;
	}

	public Collection<Odds> getOdds() {
		return odds;
	}

	public void add(final Odds child) {
		child.setMatch(this);
		this.getOdds().add(child);
	}

}

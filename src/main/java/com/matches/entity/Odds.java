package com.matches.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.matches.enumeration.Specifier;

@Entity
@Table(name = "odds")
public class Odds {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MATCH_ID")
	@JsonIgnore
	private Match match;

	@Column(name = "SPECIFIER")
	private Specifier specifier;

	@Column(name = "ODDS")
	private Double odds;

	public Odds(final Match match, final Specifier specifier, final Double odds) {
		this.match = match;
		this.specifier = specifier;
		this.odds = odds;
	}

	public Odds() {
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public Match getMatch() {
		return match;
	}

	public void setMatch(final Match match) {
		this.match = match;
	}

	public Specifier getSpecifier() {
		return specifier;
	}

	public void setSpecifier(final Specifier specifier) {
		this.specifier = specifier;
	}

	public Double getOdds() {
		return odds;
	}

	public void setOdds(final Double odds) {
		this.odds = odds;
	}

}

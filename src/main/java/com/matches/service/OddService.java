package com.matches.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.matches.entity.Match;
import com.matches.entity.Odds;
import com.matches.enumeration.Specifier;
import com.matches.manager.MatchManager;
import com.matches.manager.OddManager;
import com.matches.model.OddDto;

@Service
public class OddService {

	@Autowired
	private OddManager oddManager;

	@Autowired
	private MatchManager matchManager;

	@Transactional
	public Match insertOddToMatch(OddDto oddDto) throws Exception {

		Match match = matchManager.getMatchById(oddDto.getMatchId().longValue());
		match.add(new Odds(match, Specifier.getByCode(oddDto.getSpecifier()), oddDto.getOdds()));

		return matchManager.registerOrUpdate(match);
	}

	@Transactional
	public Odds updateOdd(Integer id, OddDto oddDto) throws Exception {
		Odds odds = oddManager.getMatchById(id.longValue());
		odds.setSpecifier(Specifier.getByCode(oddDto.getSpecifier()));
		odds.setOdds(oddDto.getOdds());
		return oddManager.registerOrUpdate(odds);
	}

	@Transactional
	public Long removeOdd(Integer id) throws Exception {
		return oddManager.removeOdd(id);
	}

}

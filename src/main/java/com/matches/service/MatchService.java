package com.matches.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.matches.entity.Match;
import com.matches.entity.Odds;
import com.matches.enumeration.Sport;
import com.matches.manager.MatchManager;
import com.matches.model.MatchDto;
import com.matches.model.MatchResponseDto;
import com.matches.model.MatchResultDto;
import com.matches.repository.MatchRepository;

@Service
public class MatchService {

	@Autowired
	private MatchRepository matchRepository;

	@Autowired
	private MatchManager matchManager;

	public MatchResponseDto getMatches(Integer page, Integer limit, String sort) {


		/* Create pageable from query params*/
		final Pageable pageable;
		if (sort != null && !sort.isEmpty()) {
			pageable = PageRequest.of(page - 1, limit, sort.toUpperCase().equals("ASC") ? Sort.by("id").ascending() : Sort.by("id").descending());
		} else {
			pageable = PageRequest.of(page - 1, limit);
		}

		Page<Match> matchPage = matchManager.getMatches(pageable);

		List<MatchResultDto> matchResultDtoList = matchPage.getContent().stream().map(this::transformMatchToMatchResultDto).collect(Collectors.toList());

		return new MatchResponseDto(matchPage.getTotalElements(), matchPage.getTotalPages(), matchResultDtoList);
	}

	public MatchResultDto transformMatchToMatchResultDto(Match match) {
		MatchResultDto matchResultDto = new MatchResultDto();
		matchResultDto.setId(match.getId());
		matchResultDto.setDescription(match.getDescription());
		matchResultDto.setMatchDate(match.getMatchDate().toString());
		matchResultDto.setMatchTime(match.getMatchTime().toString());
		matchResultDto.setTeamA(match.getTeamA());
		matchResultDto.setTeamB(match.getTeamB());
		matchResultDto.setSport(match.getSport());
		return matchResultDto;
	}

	public Match createMatch(MatchDto matchDto) {

		LocalDate date = getLocalDate(matchDto.getMatchDate());
		LocalTime time = getLocalTime(matchDto.getMatchTime());

		Match match = new Match();
		return appendMatchDetails(matchDto, date, time, match);

	}

	public Match updateMatch(Long id, MatchDto matchDto) throws Exception {

		LocalDate date = getLocalDate(matchDto.getMatchDate());
		LocalTime time = getLocalTime(matchDto.getMatchTime());

		Match match = matchManager.getMatchById(id);
		return appendMatchDetails(matchDto, date, time, match);

	}

	public Long removeMatch(Long id) {
		return matchManager.removeMatch(id);

	}

	private LocalTime getLocalTime(String timeStr) {
		final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
		LocalTime time = LocalTime.now();
		if (timeStr != null) {
			time = LocalTime.parse(timeStr, timeFormatter);
		}
		return time;
	}

	private LocalDate getLocalDate(String dateStr) {
		final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate date = LocalDate.now();
		if (dateStr != null) {
			date = LocalDate.parse(dateStr, dateFormatter);
		}
		return date;
	}

	private Match appendMatchDetails(final MatchDto matchDto, final LocalDate date, final LocalTime time, final Match match) {
		match.setDescription(matchDto.getDescription());
		match.setMatchDate(date);
		match.setMatchTime(time);
		match.setTeamA(matchDto.getTeamA());
		match.setTeamB(matchDto.getTeamB());
		match.setSport(Sport.getByCode(matchDto.getSportId()));

		return matchManager.registerOrUpdate(match);
	}

	public List<Odds> getOddsByMatchId(Integer matchId) {

		List<Match> matches = matchRepository.findAllById(matchId.longValue());

		return new ArrayList<>(matches.get(0).getOdds());
	}

}

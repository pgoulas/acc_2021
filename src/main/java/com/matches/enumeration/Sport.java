package com.matches.enumeration;

public enum Sport {
	FOOTBALL(1, "Football"),
	BASKETBALL(2, "Basketball");

	private Integer id;
	private String description;

	Sport(final Integer id, final String description) {
		this.id = id;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public static Sport getByCode(final Integer code) {
		for (final Sport sport : Sport.values()) {
			if (sport.getId().equals(code)) {
				return sport;
			}
		}
		return null;
	}
}

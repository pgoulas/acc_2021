package com.matches.enumeration;

public enum Specifier {
	HOME(1, "Home"),
	AWAY(2, "Away"),
	DRAW(3, "Draw");

	private final Integer id;
	private final String description;

	Specifier(final Integer id, final String description) {
		this.id = id;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public static Specifier getByCode(final Integer code) {
		for (final Specifier specifier : Specifier.values()) {
			if (specifier.getId().equals(code)) {
				return specifier;
			}
		}
		return null;
	}
}

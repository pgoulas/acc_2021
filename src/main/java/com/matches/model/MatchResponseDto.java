package com.matches.model;

import java.util.List;

public class MatchResponseDto {

	private Long totalElements;
	private Integer totalPages;
	private List<MatchResultDto> matchResults;

	public MatchResponseDto(final Long totalElements, final Integer totalPages, final List<MatchResultDto> matchResults) {
		this.totalElements = totalElements;
		this.totalPages = totalPages;
		this.matchResults = matchResults;
	}

	public MatchResponseDto() {
	}

	public Long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(final Long totalElements) {
		this.totalElements = totalElements;
	}

	public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(final Integer totalPages) {
		this.totalPages = totalPages;
	}

	public List<MatchResultDto> getMatchResults() {
		return matchResults;
	}

	public void setMatchResults(final List<MatchResultDto> matchResults) {
		this.matchResults = matchResults;
	}
}

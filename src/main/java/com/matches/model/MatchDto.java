package com.matches.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

public class MatchDto {

	@ApiModelProperty(
		value = "The description of the match",
		required = true,
		example = "OSFP-PAO")
	@NotNull
	@NotEmpty
	private String description;

	@ApiModelProperty(
		value = "The date of the match",
		required = true,
		example = "31/03/2021")
	@NotNull
	@NotEmpty
	private String matchDate;

	@ApiModelProperty(
		value = "The time of the match",
		required = true,
		example = "12:00")
	@NotNull
	@NotEmpty
	private String matchTime;

	@ApiModelProperty(
		value = "The name of the host",
		required = true,
		example = "OSFP")
	@NotNull
	@NotEmpty
	private String teamA;

	@ApiModelProperty(
		value = "The name of the guest",
		required = true,
		example = "PAO")
	@NotNull
	@NotEmpty
	private String teamB;

	@ApiModelProperty(
		value = "The id that refers to each sport(1.Football, 2.Basketball)",
		required = true,
		example = "1")
	@NotNull
	@NotEmpty
	private Integer sportId;

	public MatchDto(final String description, final String matchDate, final String matchTime, final String teamA, final String teamB, final Integer sportId) {
		this.description = description;
		this.matchDate = matchDate;
		this.matchTime = matchTime;
		this.teamA = teamA;
		this.teamB = teamB;
		this.sportId = sportId;
	}

	public MatchDto() {
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getMatchDate() {
		return matchDate;
	}

	public void setMatchDate(final String matchDate) {
		this.matchDate = matchDate;
	}

	public String getMatchTime() {
		return matchTime;
	}

	public void setMatchTime(final String matchTime) {
		this.matchTime = matchTime;
	}

	public String getTeamA() {
		return teamA;
	}

	public void setTeamA(final String teamA) {
		this.teamA = teamA;
	}

	public String getTeamB() {
		return teamB;
	}

	public void setTeamB(final String teamB) {
		this.teamB = teamB;
	}

	public Integer getSportId() {
		return sportId;
	}

	public void setSportId(final Integer sportId) {
		this.sportId = sportId;
	}
}

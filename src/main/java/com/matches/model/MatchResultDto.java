package com.matches.model;

import com.matches.enumeration.Sport;

public class MatchResultDto {

	private Long id;

	private String description;

	private String matchDate;

	private String matchTime;

	private String teamA;

	private String teamB;

	private Sport sport;

	public MatchResultDto(final Long id, final String description, final String matchDate, final String matchTime, final String teamA, final String teamB, final Sport sport) {
		this.id = id;
		this.description = description;
		this.matchDate = matchDate;
		this.matchTime = matchTime;
		this.teamA = teamA;
		this.teamB = teamB;
		this.sport = sport;
	}

	public MatchResultDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getMatchDate() {
		return matchDate;
	}

	public void setMatchDate(final String matchDate) {
		this.matchDate = matchDate;
	}

	public String getMatchTime() {
		return matchTime;
	}

	public void setMatchTime(final String matchTime) {
		this.matchTime = matchTime;
	}

	public String getTeamA() {
		return teamA;
	}

	public void setTeamA(final String teamA) {
		this.teamA = teamA;
	}

	public String getTeamB() {
		return teamB;
	}

	public void setTeamB(final String teamB) {
		this.teamB = teamB;
	}

	public Sport getSport() {
		return sport;
	}

	public void setSport(final Sport sport) {
		this.sport = sport;
	}
}

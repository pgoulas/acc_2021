package com.matches.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

public class OddDto {

	@ApiModelProperty(
		value = "The id of the match",
		required = true,
		example = "1")
	@NotNull
	@NotEmpty
	private Integer matchId;

	@ApiModelProperty(
		value = "The name of the guest",
		required = true,
		example = "1.2")
	@NotNull
	@NotEmpty
	private Double odds;

	@ApiModelProperty(
		value = "The id that refers to each sport(1.Home, 2.Away, 3.Draw)",
		required = true,
		example = "1")
	@NotNull
	@NotEmpty
	private Integer specifier;

	public Integer getMatchId() {
		return matchId;
	}

	public void setMatchId(final Integer matchId) {
		this.matchId = matchId;
	}

	public Double getOdds() {
		return odds;
	}

	public void setOdds(final Double odds) {
		this.odds = odds;
	}

	public Integer getSpecifier() {
		return specifier;
	}

	public void setSpecifier(final Integer specifier) {
		this.specifier = specifier;
	}
}
